#include <string>
#include <iostream>
using namespace std;

struct Node
{
	string name;
	int16_t random_int; 
	uint16_t score; 
	Node * next;

};

class List
{
	private:

		Node * head;
		Node * tail;

	public:

		//our constructor
		List();
		void createNode(string name_value, int16_t random_int_value, uint16_t score_value);

		void insertAtFront(string name_value, int16_t random_int_value, uint16_t score_value);
		void insertAtBack(string name_value, int16_t random_int_value, uint16_t score_value);
		void insertAtPosition(string name_value, int16_t random_int_value, uint16_t score_value);

		void deleteAtFront();
		void deleteAtBack();
		void deleteAtPosition(int index);

		void reverse();

		void display();

};

