GPP      = g++ -std=gnu++14

all : civil_maps_test

civil_maps_test : cm.o linkedlist.o
			${GPP} cm.o linkedlist.o -o civil_maps_test

%.o : %.cpp
	${GPP} -c $<

spotless : clean
	- rm civil_maps_test

clean :
	- rm *.o

cm.o: cm.cpp linkedlist.h
linkedlist.o: linkedlist.h linkedlist.cpp