# Civil_Maps_CPP

Hello! Here is my C++ program to manipulate the data set that you linked.

# Build

I've included a Makefile to make your life easier! Make sure you're in the directory where the source code resides 
and run the following commands:

	make
	./civil_maps_test
	
# Clean

To clean up the directory call the following commands:

	make clean
	make spotless
