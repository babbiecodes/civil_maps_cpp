#include <string>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <sstream>
#include <stdlib.h>
#include "linkedlist.h"
using namespace std;

// Our 'Customer' struct to represent the data. 
struct Customer
{
	char * name;
	int16_t random_int; 
	uint16_t score; 

};

// Set the initial number of customers we want to allocate to 5 - reallocation is supported! 
int INITIAL_CAPACITY = 5;
int USED = 0;
int TOTAL_CUSTOMERS = 0;

// Dynamically allocate array of pointers that point to however many structs we need.
Customer ** cm_user_ptr_array = (Customer **) malloc(sizeof(Customer)*INITIAL_CAPACITY);

// Our function prototypes. 
void build_data();
void print_palindrome();
bool is_palindrome(const char * name);
void stringRemoveNonAlphaNum(char * str);
void print_largest_subarray();
void build_linked_list();

// Main
int main() 
{
   
   // Function calls that handle everything we need. 
   build_data();
   print_palindrome();
   print_largest_subarray();
   build_linked_list();

   // Loop through cm_user and delete each pointer to a struct
   for (int i = 0; i < TOTAL_CUSTOMERS; i++)
   {
   		delete(cm_user_ptr_array[i]);
   }

   // Free the memory we allocated for our struct. 
   free(cm_user_ptr_array); 

   return 0;
}

// Builds our pointer array as a linked list. 
void build_linked_list()
{
	 // Declare our linked list. 
   List myList; 

   // Local temp variables for our list. 
   string temp_name = "";
   int16_t temp_random_int;
   uint16_t temp_score;

   // Build our linked list. 
   for (int i = 0; i < TOTAL_CUSTOMERS; i++)
   {
   		temp_name = cm_user_ptr_array[i]->name;
   		temp_random_int = cm_user_ptr_array[i]->random_int;
   		temp_score = cm_user_ptr_array[i]->score;
   		
   		myList.createNode(temp_name, temp_random_int, temp_score);
   }

   // Print out the linked list before reversing.
   cout << "\n *** HERE IS THE ARRAY AS A LINKED LIST ***\n" << endl;
   myList.display();

   // Print out the linked list after reversing. 
   cout << "\n *** HERE IS THE LINKED LIST REVERSED ***\n" << endl;
   myList.reverse();
   myList.display();

}

// Find the indices where we get the largest sum of the random_int elements. 
// Since we are doing it in one pass, the time compleity is O(n).
void print_largest_subarray()
{
  
  // Declare our variables. 
  int begin, end, current, previous, potential_begin;
  begin = 0;
  end = 0;
  current = 0;
  previous = 0;
  potential_begin = 0;

  // Set previous to our first element.
  previous = cm_user_ptr_array[0]->random_int;

  for(int i = 0; i < TOTAL_CUSTOMERS; i++)
  {
  		// Add the first element to our current maxinum sum.
	  	current += cm_user_ptr_array[i]->random_int;

	  	// If our sum goes below zero, we want to be greedy and reset our current max and end index pointer.
	    if(current < 0)
	    {
	    	current = 0;
	        begin = i + 1;
	        
	    }
	    // If our current max is larger then our previous, we want to reset our current begin pointer.
	    else if(current > previous)
	    {
	        end = i;
	        potential_begin = begin;
	        previous = current;
	    }

    }

    cout << "\n *** HERE IS LARGEST SUM SUBARRAY ELEMENTS ***\n" << endl;

    for (int i = potential_begin; i <= end; i++)
    {
    	cout << cm_user_ptr_array[i]->random_int << " ";

    }

    cout << endl;

}

// Helper function to remove non alpha numeric characters in a string. 
// https://gist.github.com/Morse-Code/5310046
void stringRemoveNonAlphaNum(char * str)
{

    unsigned long i = 0;
    unsigned long j = 0;
    char c;

    while ((c = str[i++]) != '\0')
    {
        if (isalnum(c))
        {
            str[j++] = c;
        }
    }

    str[j] = '\0';

}

// Function to determine if our names are palindromes or not. 
bool is_palindrome(char * name)
{

	char * temp_name = (char *) calloc(strlen(name), 1);
	strcpy(temp_name, name);
	
	for (int i = 0; i < strlen(temp_name); ++i) temp_name[i] = tolower(temp_name[i]);

	stringRemoveNonAlphaNum(temp_name);

	int begin = 0;
    int end = strlen(temp_name) - 1;
 
    // Keep comparing characters while they are same
    while (end > begin)
    {
        if (temp_name[begin++] != temp_name[end--])
        {
        	free(temp_name);     
            return false; 
        }
    }

	free(temp_name);

	return true;

}

// Helper function to print the palindromes.
void print_palindrome()
{

	cout << "\n *** ELEMENT NAMES THAT ARE PALINDROMES *** \n\n";

	for (int i = 0; i < TOTAL_CUSTOMERS; i++)
	{
		if (is_palindrome(cm_user_ptr_array[i]->name))
		{
			cout << cm_user_ptr_array[i]->name << " " <<  cm_user_ptr_array[i]->random_int << " " << cm_user_ptr_array[i]->score << endl;

		} 
	}

	cout << endl;

}

// Parses our data file to build the array pointer. 
void build_data()
{

	// Open our file for input using fstream. 
	ifstream infile;
	infile.open ("data2.txt");

	// String placeholders to parse our data file. 
	string name;
	string random_int;
	string score;

	// Int placeholders.
	int current_rand_int;
	int current_score;

	// Need a struct for each customer. 
	Customer * current_customer;

	// Declare our counter. 
	int i = 0;

	// Parse the file until we reach the end of file. 
	while (!infile.eof())
	{

		// Check to see if we have reached our capacity.
		if (USED == INITIAL_CAPACITY)
		{
			
			// Double our current capacity so we can accomodate more pointers to structs. 
			INITIAL_CAPACITY *= 2; 
			cm_user_ptr_array = (Customer **) realloc(cm_user_ptr_array, sizeof(Customer *)*INITIAL_CAPACITY);

		}

		// Allocate a new Customer struct.
		current_customer = new Customer;

		// Store the data from each line into strings at first using getline().
		getline(infile, name);
		getline(infile, random_int);
		getline(infile, score);

		// Convert our current random_int from String to int. 
		current_rand_int = stoi(random_int);
		current_score = stoi(score);

		// Get the total number of characters of our name (added the +1 for end of string char).
		int string_len_name = name.length() + 1;

		// Allocate the number of characters we need (string length * bytes of char).
		// Then copy it to our new char * 
		char * current_name = (char *) calloc(string_len_name, 1);
		strcpy(current_name, name.c_str());

		// Set the end of string char. 
		current_name[string_len_name] = '\0';

		// Set the appropriate values to our struct. 
		current_customer->name = current_name;
		current_customer->random_int = current_rand_int;
		current_customer->score = current_score; 

		// Point to the newly created struct in our array. 
		cm_user_ptr_array[i] = current_customer; 

		// Increment counts. 
		i++;
		USED++;

	}

	// Set TOTAL_CUSTOMERS to however many we found in the file. 
	TOTAL_CUSTOMERS = i;

	infile.close();

}
