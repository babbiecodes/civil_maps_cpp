#include "linkedlist.h"

List::List()
{
	head = NULL;
	tail = NULL;
}

void List::createNode(string name_value, int16_t random_int_value, uint16_t score_value)
{
	Node * temp = new Node; 
	temp->name = name_value;
	temp->random_int = random_int_value;
	temp->score = score_value;

	temp->next = NULL;

	//There is nothing in our List which allows us to make the head our new node.
	if (head == NULL)
	{
		head = temp;
		tail = temp;
		temp = NULL;
	}
	// If there is already a head, we will change the current tail to point to our 
	// newly created node. Then we set our new node as the tail.
	else
	{
		tail->next = temp;
		tail = temp;
	}
}

void List::insertAtFront(string name_value, int16_t random_int_value, uint16_t score_value)
{
	Node * temp = new Node;
	temp->name = name_value;
	temp->random_int = random_int_value;
	temp->score = score_value;

	//Since we are inserting at the front, we set our new node's pointer to the current head
	temp->next = head;

	//Then set our new node equal to the head (since the rest of the List has been untouched we can do this)
	head = temp;
}

void List::deleteAtFront()
{
	// Declare new node and set it to the head. 
	Node * temp = head;
	//temp = head;

	head = head->next;

	delete temp; 
}

void List::deleteAtBack()
{

	Node * current = head;
	Node * previous = new Node; 

	//current = head;

	//Traverse to get to the tail node and then set the tail to
	//the previous node before it. 
	while (current->next != NULL)
	{
		previous = current;

		current = current->next;

	}

	tail = previous;
	previous->next = NULL;

	delete current; 


}

void List::deleteAtPosition(int index)
{
	Node * current = head;
	Node * previous = new Node;

	for (int i = 0; i < index; i++)
	{
		previous = current;
		current = current->next;

	}

	previous->next = current->next; 

	delete previous; 

}

void List::reverse()
{
	if(head == NULL) return;

    Node *prev = NULL, *current = NULL, *next = NULL;
    current = head;

    while(current != NULL)
    {
        next = current->next;
        current->next = prev;
        prev = current;
        current = next;
    }
    // let head point to the last node
    head = prev;

}

void List::display()
{
	Node * temp = head;

	while(temp != NULL)
	{
		cout << temp->name << endl;

		temp = temp->next; 
	}

}